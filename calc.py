class InvalidExpression(Exception):
    pass


class Calc():
    # priority = {
    #     '+': 1,
    #     '-': 1,
    #     '*': 2,
    #     '/': 2,
    #     '^': 3,
    # }
    operation = {
        '+': lambda x, y: x + y,
        '-': lambda x, y: x - y,
        '*': lambda x, y: x * y,
        '/': lambda x, y: x / y if y != 0 else ZeroDivisionError,
        '^': lambda x, y: x ^ y,
    }

    def __init__(self, line):
        self.number_stack = []
        self.operation_stack = []
        self.line = line

    def separate(self):  # раскидываем числа и знаки по стекам
        separator_list = list(self.operation.keys())
        number = ''
        for symbol in self.line:
            if symbol in separator_list:
                self.operation_stack.append(symbol)
                if isinstance(float(number), float):
                    self.number_stack.append(float(number))
                    number = ''
                else:
                    raise InvalidExpression('В мат. выражении не должны содержаться посторонние символы!')
            else:
                number += symbol
        if isinstance(float(number), float):
            self.number_stack.append(float(number))

    def calculate(self):
        self.separate()
        x = self.number_stack.pop()
        y = self.number_stack.pop()
        operation = self.operation_stack.pop()
        result = self.operation[operation](y, x)
        return result


line = input('Введите математическое выражение(пока что 2 числа и между ними знак операции): ')
calc = Calc(line=line)
print(calc.calculate())
